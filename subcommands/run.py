"""
usage: reproduce run [options]  [--] <command>

Options:
  --template <template>  the name of the file(s) that should be treated
                         as a template inside the command;
                         for multiple templates separate with commas
  --src <sourcedir>      check the source code at <sourcedir>
  --build                build the code from source using `make install`
  --hash <oldhash>       pre-load parameters from this run
  --show                 print out rendered template
  --save                 save the rendered template to file
  --list-parameters      list all parameters that need to be set
  -p <key:value>         for several parameters use k1:v1,k2:v2 syntax
  --help                 print this information

Currently supported platform types: desktop (default) and SLURM.

hash can be 'latest' to refer to the latest saved run or 'current'.

Parameters that will be automatically replaced:
  random_seed: random 32 bit unsigned integer
  JOBS:  number of parallel jobs (available threads-1 for desktops)
  hash:  the hash of this run in the log file
  shorthash:  a shorter version of the hash

List all entries in the log file or just the one related to <hash>.

For common options for 'reproduce' see 'reproduce --help'.

"""

import datetime
import hashlib
import os
from pathlib import Path
import re
import subprocess
import sys
import tempfile
import time

from jinja2 import Template
import numpy as np
try:
    import ujson as json
except ModuleNotFoundError:
    import json

from subcommands.helper import (get_git_commit, git_is_clean, git_head,
                                latest, get_duration)


def guess_template(command):
    """Make an educated guess for the template name for python and mcnp calculations"""

    # check if last element of command is a file and if so,
    # if it includes any {{ }}
    possible_templates = []

    # check if we are running an mcnp simulation where the input file is given by 'i=<inputfile>'
    for c in command:
        if c.startswith('i='):
            possible_templates.append(c[2:])
    # also check the last entry in the command line (e.g. this might work for python calls)
    possible_templates.append(command[-1])

    # test if files exist and if they include some jinja code {{ }} for templates
    for template in possible_templates:
        if Path(template).exists():
            jinja2_pattern = re.compile(r'\{\{.*?\}\}')
            with open(template, 'r') as f:
                is_template = False
                for line in f:
                    match = jinja2_pattern.findall(line)
                    if match:
                        is_template = True
            if is_template:
                return template  # return the first hit
    return None


def check_template(template, command, taskinfo):
    """Check the template file for errors, scan for parameters and other information"""

    logger = taskinfo.logger

    template_use = {}
    parameters = {}
    template_in_commit = None

    if not template:
        template = guess_template(command)

    if template:
        if not Path(template).exists():
            logger.error("Can't find path to input file " + template)
            sys.exit(2)
        current_dir = Path.cwd()
        template_in_commit = get_git_commit(current_dir, template)
        taskinfo.logger.debug('template commit: {}'.format(template_in_commit))
        if template_in_commit is None:
            logger.error('Input file ' + template + ' is not in git')
            if not taskinfo.DEVEL:
                sys.exit(3)
        taskinfo.logger.debug("template file is in commit: %s",
                              template_in_commit)

        # get list of parameters we need to substitute
        jinja2_pattern = re.compile(r'\{\{.*?\}\}')
        ptrac_pattern = re.compile(r'^\s*ptrac')
        dask_patterns1 = re.compile(r'^\s*import dask')
        dask_patterns2 = re.compile(r'^\s*from dask.* import')
        with open(template, 'r') as f:
            for line in f:
                # also check for other options, such as the use of
                # dask or mcnp ptrac
                if dask_patterns1.match(line):
                    template_use['start_dask'] = True
                elif dask_patterns2.match(line):
                    template_use['start_dask'] = True
                elif ptrac_pattern.match(line):
                    template_use['single_thread'] = True
                match = jinja2_pattern.findall(line)
                if match:
                    for m in match:
                        parameters[m[2:-2].strip()] = None
        match = jinja2_pattern.findall(' '.join(command))
        if match:
            for m in match:
                parameters[m[2:-2].strip()] = None

    return template, parameters, template_use, template_in_commit


def check_src(srcdir, taskinfo):
    """Check the status and hash of code in the given source code directory"""
    logger = taskinfo.logger
    logger.debug("Checking source directory: " + srcdir)
    # Check that the directory exists
    if not Path(srcdir).is_dir():
        logger.error("Source directory not found: " + srcdir)
        sys.exit(2)
    # Check that the directory is clean
    try:
        src_is_clean = git_is_clean(srcdir)
    except OSError as e:
        logger.error("Could not check status of source directory: "
                     + srcdir + '\n'
                     + str(e))
        sys.exit(2)
    if src_is_clean:
        logger.debug("Source directory reported Git status clean.")
    else:
        logger.error("Source directory is not clean: " + srcdir + '\n' +
                     "Please check Git status before rerunning.")
        sys.exit(2)
    # Check the current hash at the HEAD of the source code repo
    try:
        src_hash = git_head(srcdir)
    except OSError as e:
        logger.error("Could not get latest commit from source directory: "
                     + srcdir + '\n'
                     + str(e))
        sys.exit(2)
    if not src_hash:
        logger.error("Missing commit details in source directory: "
                     + srcdir + '\n' +
                     "Please check Git status before rerunning.")
        sys.exit(2)
    return src_hash


def build_src(srcdir, taskinfo):
    """Build source code in the given directory, using `make install`"""
    logger = taskinfo.logger
    logger.debug("Building source code in directory: " + srcdir)
    # Check that the directory exists
    if not Path(srcdir).is_dir():
        logger.error("Source directory not found: " + srcdir)
        sys.exit(2)
    # Try to rebuild
    process = subprocess.run(['make', 'install'],
                             cwd=srcdir,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             encoding='ascii')
    # Check for errors
    if process.returncode != 0:
        logger.error('Could not run `make install` in ' + str(srcdir) + '\n'
                      + process.stderr)
        sys.exit(2)
    # Debug output
    if process.stdout:
        logger.debug('Make output:' + '\n' + process.stdout)
    return None


def run(commands, taskinfo):
    """Run the subcommand. This is the entry point for the main program"""

    logger = taskinfo.logger
    platform = taskinfo.platform
    computername = taskinfo.computername
    reproduce_commit = taskinfo.reproduce_commit
    logfile = taskinfo.logfile
    runlog = taskinfo.runlog

    # define some variables used throughout the program
    origcommand = commands['<command>']
    runcommand = origcommand.split() if origcommand else []
    template_in = commands['--template']

    # Show run info
    logger.info("Current command: " + origcommand)
    logger.info("Current directory: " + str(Path.cwd()))
    logger.info("Current time: " + time.strftime("%Y-%m-%d %H:%M:%S"))

    # are there any input files?
    if not template_in:
        template_in = guess_template(runcommand)
    parameters = {}
    template_use = {}
    template_in_commit = None
    if template_in:
        for template in template_in.split(','):
            (template_out, parameters_out, template_use_out,
                template_in_commit_out) = check_template(
                    template, runcommand, taskinfo)
            parameters.update(parameters_out)
            template_use.update(template_use_out)
            if template_in_commit_out:
                template_in_commit = template_in_commit_out

    # is there a source directory to check?
    srcdir = commands['--src']
    src_hash = None
    if srcdir:
        src_hash = check_src(srcdir, taskinfo)
        if src_hash:
            logger.info('Source code directory: ' + str(srcdir))
            logger.info('Source code commit: ' + str(src_hash))

    # rebuild code from source if required
    build = commands['--build']
    if build:
        if srcdir:
            build_src(srcdir, taskinfo)
        else:
            logger.error('Cannot build source code without knowing ' +
                         'source directory.' + '\n' +
                         'Please rerun with the option `--src` to specify ' +
                         'the source code directory.')
            sys.exit(2)

    # remember which parameters get default values
    param_default = []

    # set some default values
    if 'random_seed' in parameters:
        parameters['random_seed'] = int(np.random.random_integers(2**32-1))
        param_default.append('random_seed')
    if 'JOBS' in parameters:
        parameters['JOBS'] = max(len(os.sched_getaffinity(0))-1, 1)
        param_default.append('JOBS')

    # load parameters from old shots
    parent = None
    if commands['--hash']:
        if commands['--hash'] == 'latest':
            commands['--hash'] = latest(logfile)
        matches = 0
        with logfile.open() as f:
            for line in f.readlines():
                if '"hash": "{}'.format(commands['--hash']) in line:
                    matches += 1
                    matchedline = line
        if matches == 0:
            logger.error("Hash not found in file")
            sys.exit(1)
        if matches > 1:
            logger.error("Hash not unique")
            sys.exit(1)
        oldrun = json.loads(matchedline)
        parent = oldrun['hash']
        parameters.update(oldrun['parameters'])
        logger.debug('Added old parameters from run {}'.
                     format(commands['--hash']))

    # overwrite or replace parameters specified on the command line
    if commands['-p']:
        for pair in commands['-p'].split(','):
            s = pair.split(':')
            k = s[0]
            value = ':'.join(s[1:])
            if k not in parameters:
                logger.warning('Parameter ' + k + ' not used in input file')
            parameters[k] = value

    # print parameters if requested
    if commands['--list-parameters']:
        if parameters:
            logger.info('Parameters (*=default value used)')
            for p, v in parameters.items():
                if p in param_default:
                    logger.info(' {} -> {} *'.format(p, v))
                else:
                    logger.info(' {} -> {} '.format(p, v))
        else:
            logger.info('No parameters defined')

    # ensure all parameters are set
    problem = False
    for k, v in parameters.items():
        if k in ['hash', 'shorthash']:
            continue
        if v is None:
            logger.error('Parameter "{}" did not get a value'.format(k))
            problem = True
    if problem:
        sys.exit(3)
    # end parsing parameters

    # set the hash
    current_time = datetime.datetime.now().isoformat()
    loghash = ' '.join(runcommand) + current_time
    m = hashlib.sha1()
    m.update(loghash.encode('utf-8'))
    loghash = m.hexdigest()
    if 'hash' in parameters and parameters['hash'] is not None:
        logger.error("'hash' will always be automatically set "
                     "and can't be the name of a parameter")
        sys.exit(5)
    if 'shorthash' in parameters and parameters['shorthash'] is not None:
        logger.error("'hash' will always be automatically set "
                     "and can't be the name of a parameter")
        sys.exit(5)
    parameters['hash'] = loghash
    parameters['shorthash'] = loghash[:7]
    logger.info("Current run hash: " + loghash)

    if (commands['--show'] or commands['--save']) and template_in:
        for this_template in template_in.split(','):
            with open(this_template, 'r') as f:
                template = Template(f.read())
                rendered = template.render(parameters)
            if commands['--show']:
                logger.info(f'------rendered template for {this_template}------')
                logger.info(rendered)
                logger.info('----------------------------------------------')
            if commands['--save']:
                with open(this_template + '.rendered', 'w') as f:
                    f.write(rendered)
                    f.write('\n')


    logger.debug('Running the command')
    data = run_program(runcommand, parameters, platform,
                       template_in, template_use, logger, runlog)
    logger.debug('return data: {}'.format(data))
    jobnumber = data.pop('jobnumber', None)
    runtime = data.pop('runtime', None)
    data.pop('command', None)
    parameters.update(data)
    parameters.pop('hash')  # don't store the hash also in the parameter list
    parameters.pop('shorthash')
    logentry = {'hash': loghash,
                'time': current_time,
                'reproduce_commit': reproduce_commit,
                'computername': computername,
                'input': template_in,
                'input_commit': template_in_commit,
                'parameters': parameters,
                'command_line': origcommand}
    if parent:
        logentry['parent'] = parent
    if jobnumber:
        logentry['jobnumber'] = jobnumber
    if runtime:
        logentry['runtime'] = runtime
        logger.info("Run time: " + get_duration(runtime))
    if src_hash:
        logentry['source_dir'] = srcdir
        logentry['source_commit'] = src_hash
        if build:
            logentry['build_source'] = True
        else:
            logentry['build_source'] = False
    if not taskinfo.DEVEL:
        logger.debug('adding log entry to: {}'.format(logfile))
        with open(logfile, 'a') as f:
            f.write(json.dumps(logentry))
            f.write('\n')


def run_program(command, parameters, platform, template_in=None,
                options=None, logger=None, runlog=None):
    """
    Run the command depending on the platform.
    Record the execution time.
    """
    logger.debug('in run function')
    start = time.time()

    if platform == 'SLURM':
        data = run_slurm(command, parameters, template_in, options, logger)
    else:
        data = run_desktop(command, parameters, template_in, options, logger, runlog)

    data['runtime'] = time.time() - start
    return data

# define the run command on different platforms


def run_slurm(command, parameters, template_in, options, logger):
    """
    Create temporary input file, run it, and store in logentry
    """
    sbatchtemplate = """#!/bin/bash
# Job name:
#SBATCH --job-name={{jobname}}
#
# Partition:
#SBATCH --partition={{partition}}
#
# QoS:
#SBATCH --qos=lr_normal
#
# Account:
#SBATCH --account=pc_rootsapi
#
# Processors:
#SBATCH --{{request}}={{requestvalue}}
#
# Wall clock limit:
#SBATCH --time={{clocktime}}
#

# Run command, generated automatically by reproducible
# https://bitbucket.org/berkeleylab/reproducible

# cd into scratch
cd /global/scratch/$(whoami)/reproduce-{{ shorthash }}

module load python/3.6
export DATAPATH=/global/home/groups-sw/pc_rootsapi/MCNP/MCNP_DATA

pids=()
{{runner}}

for pid in ${pids[*]};
do
    wait ${pid} #Wait on all PIDs, this returns 0 if ANY process fails
done


"""
    single_thread = """
    srun -n1 -N1 --mem-per-cpu=$(($SLURM_MEM_PER_NODE/$SLURM_CPUS_ON_NODE)) --exclusive {{ command }} &
    pids+=($!)
    """

    parallel = """{{ command }}"""

    single_node = """for i in $(seq $SLURM_JOB_NUM_NODES); do
    srun -n1 -N1 --cpus-per-task=$SLURM_CPUS_ON_NODE --mem-per-cpu=$SLURM_MEM_PER_NODE --exclusive {{ command }} &
    pids+=($!)
done
"""

    jobs = int(parameters.get('JOBS'))
    particles = int(float(parameters.get('number_of_particles'))/jobs)
    seed = int(parameters.get('random_seed'))
    loghash = parameters.get('hash')
    shorthash = parameters.get('shorthash')
    # get reproducable random numbers
    RNgenerator = np.random.RandomState(seed)

    local_parameters = {'jobname': 'apersaud',
                        'partition': 'lr3',
                        'request': 'ntasks',
                        'requestvalue': jobs,
                        'clocktime': '0:30:00',
                        'command': ' '.join(command)}
    # overwrite with parameters from the command line
    local_parameters.update(parameters)

    location = Path('/global/scratch/{}/reproduce-{}'.
                    format(os.getlogin(), shorthash))
    location.mkdir()

    with open(template_in, 'r') as f:
        # replace parameters in input file
        inputtemplate = Template(f.read())
    jobtemplate = Template(single_thread)
    oldparameters = dict(local_parameters)

    if command[0].startswith('mcnp') and options.get('single_thread'):
        logger.debug('SLURM mcnp job with ptrac JOBS:{}'.format(jobs))
        runner = ""
        for i in range(jobs):
            # copy of commands
            mycommand = list(command)

            myparameters = dict(oldparameters)
            myparameters.update({'JOBS': jobs,
                                 'number_of_particles': particles,
                                 'random_seed': int(RNgenerator.random_integers(2**32-1))})
            rendered = inputtemplate.render(myparameters)
            filename = location/'reproduce-input-{}'.format(i)
            outfilename = 'reproduce-out-{}'.format(i)
            runfilename = 'reproduce-run-{}'.format(i)
            ptracfilename = 'reproduce-ptrac-{}'.format(i)

            with open(filename, 'w+') as fp:
                fp.write(rendered)

            myparameters['request'] = 'ntasks'
            myparameters['requestvalue'] = myparameters['JOBS']

            # replace parameters in command line
            cmdtemplate = Template(' '.join(mycommand))
            mycommand = cmdtemplate.render(parameters)
            mycommand = mycommand.split(' ')

            # replace filename
            for j, n in enumerate(mycommand):
                if str(template_in) in n:
                    mycommand[j] = mycommand[j].replace(str(template_in),
                                                        str(filename.name))
            mycommand[0] = '/global/home/groups-sw/pc_rootsapi/MCNP/MCNP_CODE/bin/'+mycommand[0]
            mycommand.insert(1, 'outp={}'.format(outfilename))
            mycommand.insert(1, 'runtpe={}'.format(runfilename))
            mycommand.insert(1, 'ptrac={}'.format(ptracfilename))
            mycommand = ' '.join(mycommand)
            runner = runner + jobtemplate.render(command=mycommand)
        logger.debug('created runner script: {}'.
                     format(runner[:min(200, len(runner))]))
    else:
        jobtemplate = Template(parallel)
        runner = jobtemplate.render(local_parameters)

    local_parameters['runner'] = runner

    template = Template(sbatchtemplate)
    rendered = template.render(local_parameters)

    logger.debug('creating sbatch script')
    with open('reproduce-{}'.format(shorthash), 'bw+') as fp:
        fp.write(rendered.encode('utf-8'))
        fp.flush()
        output = subprocess.run(['sbatch', fp.name],
                                stdout=subprocess.PIPE,
                                encoding='ascii')
        m = re.match('^Submitted batch job ([0-9]*)$', output.stdout)
        jobnumber = int(m.group(1))
        local_parameters['jobnumber'] = jobnumber
    local_parameters.pop('runner')
    return local_parameters


def run_desktop(command, parameters, template_in, options, logger, runlog):
    """
    Render the template and command line and run it :) If there are
    any parameters that need to be substituted in the template, create
    a temporary file and run that one instead. In this case make sure
    that we replace any output files with the temporary file name in
    them with the original.

    """
    logger.debug('in run_desktop')
    if parameters and template_in:
        if command[0].startswith('mcnp') and options.get('single_thread'):
            logger.info("Detected mcnp")
            logger.info("Will run each simulation in a single thread (e.g. for ptrac)")
            jobs = int(parameters.get('JOBS'))
            particles = int(float(parameters.get('number_of_particles'))/jobs)
            seed = int(parameters.get('random_seed'))
            loghash = parameters.get('hash')
            shorthash = parameters.get('shorthash')
            # get reproducable random numbers
            RNgenerator = np.random.RandomState(seed)
            # make a copy of the parameters
            oldparameters = dict(parameters)
            mydir = Path('reproduce-{}'.format(loghash))
            mydir.mkdir()
            processes = []
            affinities_available = list(os.sched_getaffinity(0))
            affinities_available = np.random.choice(affinities_available,
                                                    size=len(affinities_available),
                                                    replace=False)
            for nr, affinity in enumerate(affinities_available):
                # copy of commands
                mycommand = list(command)

                parameters = dict(oldparameters)
                parameters.update({'JOBS': 1,
                                   'number_of_particles': particles,
                                   'random_seed': int(RNgenerator.random_integers(2**32-1))})
                # replace parameters in command line
                template = Template(' '.join(mycommand))
                mycommand = template.render(parameters)
                mycommand = mycommand.split(' ')
                # replace parameters in input file(s)
                for this_template in template_in.split(','):
                    with open(this_template, 'r') as f:
                        template = Template(f.read())
                    rendered = template.render(parameters)
                    # we are in a unique directory, so might as well use
                    # non-random file names
                    with open(f'{mydir}/{this_template}-{shorthash}-{nr}', 
                              'bw+') as fp:
                        fp.write(rendered.encode('utf-8'))
                    for i, n in enumerate(mycommand):
                        if str(this_template) in n:
                            mycommand[i] = mycommand[i].replace(
                                str(this_template), Path(fp.name).name)
                logger.debug('running: {} {}'.format(mycommand, parameters))
                out = subprocess.Popen(mycommand, cwd=mydir, stdout=subprocess.PIPE)
                logger.debug('setting affinity for {} to {}'.format(out.pid, affinity))
                os.sched_setaffinity(out.pid, [int(affinity)])
                processes.append(out)
            for i, p in enumerate(processes):
                # we probably shouldn't wait and just return and send
                # the jobs really to the background and then use
                # 'reproduce monitor' or so to watch the results come
                # in
                p.wait()
        else:
            # replace parameters in command line
            template = Template(' '.join(command))
            command = template.render(parameters)
            command = command.split(' ')

            # replace parameters in input file
            tempfiles = []
            for this_template in template_in.split(','):
                template_in_command = any(this_template in c for c in command)
                with open(this_template, 'r') as f:
                    template = Template(f.read())
                    rendered = template.render(parameters)
                with tempfile.NamedTemporaryFile(dir=Path.cwd(),
                                                 prefix='reproduce-',
                                                 delete=False) as fp:
                    if not template_in_command:
                        logger.debug(f'saving template file {this_template} '
                                     f'as {fp.name}')
                        Path.cwd().joinpath(this_template).rename(fp.name)
                        logger.debug(f'saving new input file '
                                     f'as {this_template}')
                        with open(this_template, 'w') as f:
                            f.write(rendered)
                            f.write('\n')
                            f.flush()
                    else:
                        fp.write(rendered.encode('utf-8'))
                        fp.flush()
                        for i, n in enumerate(command):
                            if str(this_template) in n:
                                command[i] = command[i].replace(
                                    str(this_template), fp.name)
                    tempfiles.append({'original': this_template, 
                                      'temp': fp.name, 
                                      'in_command': template_in_command})

            # main run
            logger.debug(f'running: {command}')

            if runlog:
                runner = subprocess.Popen(command,
                                            stdout=subprocess.PIPE,
                                            stderr=subprocess.STDOUT)
                tee = subprocess.Popen(["tee", '-a', runlog],
                                        stdin=runner.stdout)
                runner.wait()
                tee.wait()
            else:
                subprocess.run(command)

            # if files get created during the run, they should
            # replace the existing ones with the original name
            for this_file in tempfiles:
                if not this_file['in_command']:
                    logger.debug(f'replacing template file '
                                 f"{this_file['original']}")
                    Path(this_file['temp']).rename(this_file['original'])
                else:
                    for f in Path('.').glob('./' + this_file['temp'] + '*'):
                        # removed temporary template file
                        if f.name == this_file['temp']:
                            os.remove(f.name)
                        # for files generated by the simulation, move file 
                        # and replace temporary name with original template name
                        f.rename(str(f).replace(this_file['temp'], 
                                                str(this_file['original'])))
    else:
        # no template or parameters given
        logger.debug('running: {}'.format(command))

        if runlog:
            runner = subprocess.Popen(command,
                                      stdout=subprocess.PIPE,
                                      stderr=subprocess.STDOUT)
            tee = subprocess.Popen(["tee", '-a', runlog],
                                   stdin=runner.stdout)
            runner.wait()
            tee.wait()
        else:
            subprocess.run(command)

    return {}

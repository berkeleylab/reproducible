"""Usage: reproduce clean [--force]

  --force   delete from logfile, otherwise just show bad entries
  --help    print this information

List all entries in the log file that don't have git commits set.

For common options for 'reproduce' see 'reproduce --help'.

"""
try:
    import ujson as json
except ModuleNotFoundError:
    import json


def run(commands, logfile):
    new = []
    dirty = False
    with logfile.open() as f:
        for line in f:
            out = json.loads(line)
            version = out.get('reproduce_commit')
            template = out.get('input')
            commit = out.get('input_commit')
            loghash = out.get('hash')
            if version is None:
                print(loghash[:7],
                      'has no version set for the reproduce command')
                dirty = True
                continue
            if template and (commit is None):
                print(loghash[:7], 'has no version set for the template file')
                dirty = True
                continue
            new.append(line)
    if commands['--force']:
        with logfile.open('w') as f:
            for line in new:
                f.write(line)
    else:
        if dirty:
            print(' Use --force to delete these from the logfile.')

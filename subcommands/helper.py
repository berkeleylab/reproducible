"""Some helper function that are used in different commands."""

import subprocess
import pathlib


def latest(logfile):
    """Return the hash of the latest simulation in the logfile"""
    with logfile.open() as f:
        lastline = f.readlines()[-1]
        out = json.loads(lastline)
        loghash = out.pop('hash')
    return loghash


def get_git_commit(repodir, filename = ''):
    """Get the latest commit for either a repository or file."""
    if repodir == '':
        return None
    if filename == '':
        if not git_is_clean(repodir):
            return None
    else:
        if not pathlib.Path(f"{repodir}/{filename}").exists():
            return None
        if not git_is_clean(repodir, filename):
            return None
    return git_head(repodir)

def git_is_clean(repodir, filename = ''):
    """Find out whether the git repository at repodir is clean."""
    if repodir == '':
        return None
    # Are we checking a single file or a whole repository?
    if filename:
        if not pathlib.Path(f"{repodir}/{filename}").exists():
            raise OSError(f"Can't find file {repodir}/{filename}")
        git_command = ['git', 'status', '--porcelain', '--', filename]
    else:
        git_command = ['git', 'status', '--porcelain']
    # Run `git status --porcelain` to check status
    git_status = subprocess.run(git_command,
                                cwd=repodir,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                encoding='ascii')
    # Check for errors
    if git_status.returncode != 0:
        raise OSError('Could not run `git status` in ' + str(repodir) + '\n'
                      + git_status.stderr)
    # Check whether result is clean
    if git_status.stdout == '':
        return True
    else:
        return False
    return None

def git_head(repodir):
    """Find out the current commit hash for a git repository at repodir."""
    if repodir == '':
        return None
    # Run `git rev-parse HEAD` to get hash at current `HEAD`
    git_rev = subprocess.run(['git', 'rev-parse', 'HEAD'],
                             cwd=repodir,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             encoding='ascii')
    # Check for errors
    if git_rev.returncode != 0:
        raise OSError('Could not run `git rev-parse` in ' + str(repodir) + '\n'
                      + git_rev.stderr)
    # Return the hash if found
    if git_rev.stdout:
        return git_rev.stdout[:-1]
    return None

def get_duration(seconds):
    """Convert a duration in seconds into a nice string"""
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    s = int(s)
    m = int(m)
    h = int(h)
    outputstr = ""
    if h > 0:
        outputstr += str(h) + " hour"
        if h > 1:
            outputstr += "s"
    if m > 0:
        if outputstr:
            outputstr += ", "
        outputstr += str(m) + " minute"
        if m > 1:
            outputstr += "s"
    if s > 0:
        if outputstr:
            outputstr += " and "
        outputstr += str(s) + " second"
        if s > 1:
            outputstr += "s"
    if seconds < 1:
        outputstr = "less than a second"
    return outputstr

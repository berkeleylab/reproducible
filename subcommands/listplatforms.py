"""Usage: reproduce listplatforms [--help]

  --help  print this information

List the different platforms that are configured and the regular
expression used to match the platform from the computer name.

For common options for 'reproduce' see 'reproduce --help'.

"""


def run(config, inifile):
    first = True
    if 'platforms' in config.sections():
        if first:
            print('The following platforms are configured:')
            first = False
        for regexp in config['platforms']:
            print('      {}: {}'.format(config['platforms'][regexp], regexp))
        print('    default: desktop')
    else:
        print('Currently there are no platforms configured.')
    print('The config file is located at:', inifile)

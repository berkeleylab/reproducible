"""Usage: reproduce log [--help] [-n <N>] [<hash>]

  -n <N>   only list the last N log entries
  --help   print this information

List all entries in the log file or just the one related to <hash>.

For common options for 'reproduce' see 'reproduce --help'.

"""

import pprint
try:
    import ujson as json
except ModuleNotFoundError:
    import json


def run(commands, logfile):
    with logfile.open() as f:
        lines = f.readlines()
    N = int(commands['-n']) if commands['-n'] else len(lines)
    if len(lines) < N:
        N = len(lines)
    for l in lines[-N:]:
        out = json.loads(l)
        loghash = out.pop('hash')
        if ((commands['<hash>'] and loghash.startswith(commands['<hash>'])) or
                not commands['<hash>']):
            print('hash:', loghash)
            pprint.pprint(out)
            print('-'*30)

"""Usage: reproduce listrepos [--help]

  --help  print this information

List the repositories that are configured. Each repository needs the
full path to its location and can optionally also get the name of the
log file.

For common options for 'reproduce' see 'reproduce --help'.

"""


def run(config, inifile):
    first = True
    if not config.sections():
        print("No repositories are configured yet. "
              "Use 'reproduce addrepo <alias> <path>' to add.")
    for name in config.sections():
        if first:
            print('The following repositories are configured:')
            first = False
        if name == 'platforms':
            continue
        print(' ', name)
        print('      path:', config.get(name, 'path'))
        print('      logfile:', config.get(name, 'log'))
    print('The config file is located at:', inifile)

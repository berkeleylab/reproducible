"""usage: reproduce addrepo [--force] [--help] <alias> <path>

  --force   overwrite an existing alias
  --help    print this information

For common options for 'reproduce' see 'reproduce --help'.

"""
from pathlib import Path


def run(commands, config, logfile, inifile):
    path = Path(commands['<path>'])
    alias = commands['<alias>']
    force = commands['--force']
    if alias == 'platform':
        print("Error: can't use 'platform' as a repo name, "
              "since this name is reservered for other options.")
        return 1
    if alias in config and not force:
        print('Warning: Repo already exist, use --force to overwrite it')
    else:
        if path.is_dir():
            config[alias] = {}
            config[alias]['path'] = str(path)
            config[alias]['log'] = str(logfile.name)
            with open(inifile, 'w+') as configfile:
                config.write(configfile)
        else:
            print('Error: path does not exist')
            return 1

    return 0
